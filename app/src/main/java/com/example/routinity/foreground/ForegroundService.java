package com.example.routinity.foreground;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.example.routinity.MainActivity;
import com.example.routinity.R;

/**
 * Represents a Foreground Service which runs in foreground when the app is launched
 */
public class ForegroundService extends  Service {

    // ID of the NotificationChannel
    private static final String CHANNEL_ID = "ForegroundServiceChannel";

    /**
     * Start the ForegroundService
     * @param intent
     * @param flags
     * @param startId
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Create a NotificationChannel for the ForegroundService
        createNotificationChannel();
        // Define an intent for the notification
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, 0);
        // Set parameters for the notification that has to be displayed
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(getString(R.string.title_foreground))
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();
        // Start the Foreground Service
        startForeground(1, notification);
        // Don not allow the system to recreate the service
        // unless there are pending intents to deliver
        return START_NOT_STICKY;
    }

    /**
     * Binder to interact with the Service class
     * @param intent
     * @return null
     */
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * Creating the Foreground Service
     */
    @Override
    public void onCreate() {
        super.onCreate();
    }


    /**
     * Function to destroy the Foreground Service
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    /* Create a NotificationChannel to notify to the user that
       a Foreground Service is running */
    private void createNotificationChannel() {
        // Check if API 26+ (NotificationChannel class is not in the support library)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Set parameters fot the NotificationChannel (ID, NAME, IMPORTANCE)
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    getString(R.string.app_name),
                    NotificationManager.IMPORTANCE_DEFAULT
            );
            // Get an handle to the NotificationManager class
            NotificationManager manager = getSystemService(NotificationManager.class);
            // Create the NotificationChannel which notification can be posted
            manager.createNotificationChannel(serviceChannel);
        }
    }
}
