package com.example.routinity.foreground;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.example.routinity.R;


/**
 * Represents an AutoStartReceiver which allows to run the app in foreground
 * also when the phone is rebooted
 */
public class AutoStartReceiver extends BroadcastReceiver  {

    /**
     * Start the Foreground Service at boot time
     * @param context
     * @param intent
     */
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onReceive(Context context, Intent intent) {
        // Check when the system boots
        if (Intent.ACTION_BOOT_COMPLETED.equals(intent.getAction())) {
            // Create an intent to start the Foreground Service
            Intent serviceIntent = new Intent(context, ForegroundService.class);
            // Display a message that the application is running
            Toast.makeText(context, R.string.title_foreground, Toast.LENGTH_LONG).show();
            // Start the Foreground Service
            context.startForegroundService(serviceIntent);
        }
    }
}
