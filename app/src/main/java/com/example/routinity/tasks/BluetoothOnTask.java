package com.example.routinity.tasks;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothManager;
import android.content.Context;

import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.enumerations.TaskEnum;

import java.util.Arrays;

public class BluetoothOnTask extends AbstractTask {

    /**
     * Constructor of the BluetoothOnTask class.
     */
    public BluetoothOnTask() {
        super(
                "Turn on Bluetooth",
                "ic_baseline_bluetooth",
                "This task turn on the Bluetooth connectivity.",
                TaskEnum.CONNECTIVITY,
                Arrays.asList(
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN
                )
        );
    }

    /**
     * Request to turn on the Bluetooth manager.
     *
     * @return Whether the request was successful or not.
     */
    @Override
    public boolean run(Context context) {
        // Get the default Bluetooth manager
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter bluetoothAdapter = bluetoothManager.getAdapter();
        boolean status = bluetoothAdapter != null && bluetoothAdapter.isEnabled();
        // Request to turn on the Bluetooth manager
        if (!status) {
            try {
                status = bluetoothAdapter.enable();
            } catch (Exception e) {
                status = false;
            }
        }
        return status;
    }
}
