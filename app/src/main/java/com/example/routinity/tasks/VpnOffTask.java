package com.example.routinity.tasks;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.os.Build;

import androidx.annotation.RequiresApi;

import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.enumerations.TaskEnum;

import java.util.Arrays;

public class VpnOffTask extends AbstractTask {

    /**
     * Constructor of the VpnOffTask class.
     */
    public VpnOffTask() {
        super(
                "Turn off VPN",
                "ic_baseline_code_off",
                "This task turn off the VPN connectivity.",
                TaskEnum.CONNECTIVITY,
                Arrays.asList(
                        Manifest.permission.BIND_VPN_SERVICE
                )
        );
    }

    /**
     * Request to turn off the VPN adapter.
     *
     * @return Whether the request was successful or not.
     */
    @RequiresApi(api = Build.VERSION_CODES.R)
    @SuppressLint("WrongConstant")
    @Override
    public boolean run(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Network activeNetwork = connectivityManager.getActiveNetwork();
        boolean status = activeNetwork != null && connectivityManager
            .getNetworkCapabilities(activeNetwork)
            .hasCapability(NetworkCapabilities.TRANSPORT_VPN);
        if (!status) {
            try {
                Intent intent = new Intent(android.provider.Settings.ACTION_VPN_SETTINGS);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                status = true;
            } catch (Exception e) {
                status = false;
            }
        }
        return status;
    }
}
