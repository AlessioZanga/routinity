package com.example.routinity.tasks;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Build;
import android.provider.Settings;

import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.enumerations.TaskEnum;

import java.util.Arrays;

public class MobileDataOnTask extends AbstractTask {

    /**
     * Constructor of the MobileDataOnTask class.
     */
    public MobileDataOnTask() {
        super(
                "Turn on Mobile Data",
                "ic_baseline_signal_cellular",
                "This task turn on the Mobile Data connectivity.",
                TaskEnum.CONNECTIVITY,
                Arrays.asList(
                        Manifest.permission.ACCESS_NETWORK_STATE
                )
        );
    }

    /**
     * Request to turn on the Mobile Data adapter.
     *
     * @return Whether the request was successful or not.
     */
    @Override
    public boolean run(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean status = connectivityManager.getActiveNetworkInfo() != null && !connectivityManager.getActiveNetworkInfo().isConnected();
        if (!status) {
            try {
                Intent intent = new Intent(Settings.ACTION_DATA_USAGE_SETTINGS);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                status = true;
            } catch (Exception e) {
                status = false;
            }
        }
        return status;
    }
}
