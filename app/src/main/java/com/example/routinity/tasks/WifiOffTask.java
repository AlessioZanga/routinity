package com.example.routinity.tasks;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;

import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.enumerations.TaskEnum;

import java.util.Arrays;

public class WifiOffTask extends AbstractTask {

    /**
     * Constructor of the WifiOffTask class.
     */
    public WifiOffTask() {
        super(
                "Turn off WiFi",
                "ic_baseline_wifi_off",
                "This task turn off the WiFi connectivity.",
                TaskEnum.CONNECTIVITY,
                Arrays.asList(
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.CHANGE_WIFI_STATE
                )
        );
    }

    /**
     * Request to turn off the WiFi manager.
     *
     * @return Whether the request was successful or not.
     */
    @Override
    public boolean run(Context context) {
        // Get the default WiFi manager
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        boolean status = !wifiManager.isWifiEnabled();
        if (!status) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                // Request to turn off the WiFi manager
                status = wifiManager.setWifiEnabled(false);
            } else {
                Intent intent = new Intent(Settings.Panel.ACTION_WIFI);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                status = true;
            }
        }
        return status;
    }
}
