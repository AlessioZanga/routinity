package com.example.routinity.tasks;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Build;
import android.provider.Settings;

import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.enumerations.TaskEnum;

import java.util.Arrays;

public class NfcOnTask extends AbstractTask {

    /**
     * Constructor of the NfcOnTask class.
     */
    public NfcOnTask() {
        super(
                "Turn on NFC",
                "ic_baseline_nfc",
                "This task turn on the NFC connectivity.",
                TaskEnum.CONNECTIVITY,
                Arrays.asList(
                        Manifest.permission.NFC
                )
        );
    }

    /**
     * Request to turn on the NFC adapter.
     *
     * @return Whether the request was successful or not.
     */
    @Override
    public boolean run(Context context) {
        NfcManager nfcManager = (NfcManager) context.getSystemService(Context.NFC_SERVICE);
        NfcAdapter nfcAdapter = nfcManager.getDefaultAdapter();
        boolean status = nfcAdapter != null && nfcAdapter.isEnabled();
        if (!status) {
            try {
                String flag = (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) ? Settings.ACTION_NFC_SETTINGS : Settings.Panel.ACTION_NFC;
                Intent intent = new Intent(flag);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);
                status = true;
            } catch (Exception e) {
                status = false;
            }
        }
        return status;
    }
}
