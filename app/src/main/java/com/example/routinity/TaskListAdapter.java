package com.example.routinity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.enumerations.TaskEnum;
import com.google.android.material.dialog.MaterialAlertDialogBuilder;

import java.util.HashMap;
import java.util.List;

public class TaskListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<TaskEnum> listGroup;
    private HashMap<TaskEnum, List<AbstractTask>> listItem;

    /**
     * Class constructor
     *
     * @param context activity context
     * @param listGroup list of groups of the list
     * @param listItem hashmap of items split up into groups
     */
    public TaskListAdapter(Context context, List<TaskEnum> listGroup, HashMap<TaskEnum, List<AbstractTask>> listItem){
        this.context = context;
        this.listGroup = listGroup;
        this.listItem = listItem;
    }

    /**
     * Getter of number of groups
     * @return number of groups
     */
    @Override
    public int getGroupCount() {
        return listGroup.size();
    }

    /**
     * Getter of child number of a group
     * @param groupPosition group position
     * @return number of child for a group
     */
    @Override
    public int getChildrenCount(int groupPosition) {
        return listItem.get(this.listGroup.get(groupPosition)).size();
    }

    /**
     * Getter of a group in the specified position
     * @param groupPosition position of the group
     * @return group in the specified position
     */
    @Override
    public Object getGroup(int groupPosition) {
        return listGroup.get(groupPosition);
    }

    /**
     * Getter of a child in a specified group and position
     * @param groupPosition group position
     * @param childPosition child position
     * @return child in a specified group in a specified position
     */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return listItem.get(listGroup.get(groupPosition)).get(childPosition);
    }

    /**
     * Getter of a group id in a specified position
     * @param groupPosition position of the group
     * @return id of the group in the specified position
     */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /**
     * Getter of a child id in a specified position and group
     * @param groupPosition position of the group
     * @param childPosition position of the child
     * @return id of the child in the specified position in a specified group
     */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    /**
     * Getter of stable ids state
     * @return false
     */
    @Override
    public boolean hasStableIds() {
        return false;
    }

    /**
     * Getter of the group view
     * @param groupPosition position of the group
     * @param isExpanded true if the group is expanded, false otherwise
     * @param convertView layout of the group
     * @param parent parent view
     * @return layout of the group
     */
    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        TaskEnum taskCategory = (TaskEnum) getGroup(groupPosition);
        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView textView = convertView.findViewById(R.id.list_group);
        textView.setText(taskCategory.toString());

        ImageView imageView = convertView.findViewById(R.id.list_group_icon);
        int id = context.getResources().getIdentifier(taskCategory.getIcon(), "drawable", context.getPackageName());
        imageView.setImageDrawable(context.getDrawable(id));

        return convertView;
    }

    /**
     * Getter of the child view
     * @param groupPosition position of the group
     * @param childPosition position of the child
     * @param isLastChild true if the item is the last child of the group, false otherwise
     * @param convertView layout of the child
     * @param parent parent view
     * @return layout of the child
     */
    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        AbstractTask task = (AbstractTask) getChild(groupPosition, childPosition);
        if (convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }

        TextView textView = convertView.findViewById(R.id.list_item_name);
        textView.setText(task.getName());

        ImageView imageView = convertView.findViewById(R.id.list_item_icon);
        int id = context.getResources().getIdentifier(task.getIcon(), "drawable", context.getPackageName());
        imageView.setImageDrawable(context.getDrawable(id));

        Button button = convertView.findViewById(R.id.list_item_button_info);
        button.setOnClickListener(v -> new MaterialAlertDialogBuilder(context)
                .setTitle(task.getName())
                .setMessage(task.getDescription())
                .setPositiveButton("GOT IT", (dialogInterface, i) -> {
                    // Do nothing, is only an acknowledgement
                })
                .show());
        return convertView;
    }

    /**
     * Getter of child selectable state
     * @param groupPosition position of the group
     * @param childPosition position of the child
     * @return true if the child is selectable, false otherwise
     */
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
