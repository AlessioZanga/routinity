package com.example.routinity.ui.routines;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.routinity.R;
import com.example.routinity.structures.Routine;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * Class for Routines fragment in main activity
 */
public class RoutinesFragment extends Fragment {

    /**
     * Called when the fragment is first created. The layout is inflated from the fragment. The
     * title in the Top App Bar (Toolbar) is set. The routine list is created and the routines are
     * passed to the adapter. If the list is empty the list is invisible and an advise compare.
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_routines, container, false);

        Objects.requireNonNull(((AppCompatActivity) getActivity()).getSupportActionBar()).setTitle(R.string.title_routines);

        List<Routine> routines = new ArrayList<>();
        RecyclerView list = root.findViewById(R.id.routine_list);
        list.setLayoutManager(new LinearLayoutManager(root.getContext()));
        list.setAdapter(new RoutineListAdapter(routines));

        CardView emptyView = root.findViewById(R.id.empty_view_card);

        if (routines.isEmpty()) {
            list.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
        else {
            list.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
        }

        return root;
    }

}
