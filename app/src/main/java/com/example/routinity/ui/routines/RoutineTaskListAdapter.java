package com.example.routinity.ui.routines;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.routinity.R;
import com.example.routinity.structures.AbstractTask;

import org.jetbrains.annotations.NotNull;

import java.util.List;


/**
 * Adapter for the task list of a single routine in the routine list.
 */
public class RoutineTaskListAdapter extends RecyclerView.Adapter<RoutineTaskListAdapter.TaskViewHolder>{

    List<AbstractTask> tasks;

    /**
     * Constructor for the task list adapter.
     * @param tasks list of tasks of the routine
     */
    public RoutineTaskListAdapter(List<AbstractTask> tasks) {
        this.tasks = tasks;
    }

    /**
     * Create the view holder for the task.
     * @param parent parent view
     * @param viewType view type
     * @return Task view holder
     */
    @NotNull
    @Override
    public RoutineTaskListAdapter.TaskViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new RoutineTaskListAdapter.TaskViewHolder(view);
    }

    /**
     * Set tasks in the list.
     * @param holder task holder
     * @param position position of the task in the list
     */
    @Override
    public void onBindViewHolder(RoutineTaskListAdapter.TaskViewHolder holder, int position) {
        holder.itemName.setText(tasks.get(position).getName());

        int id = holder.card.getResources().getIdentifier(tasks.get(position).getIcon(), "drawable", holder.card.getContext().getPackageName());
        @SuppressLint("UseCompatLoadingForDrawables") Drawable drawable = holder.card.getContext().getResources().getDrawable(id);
        holder.itemIcon.setImageDrawable(drawable);
    }

    /**
     * Get the number of tasks in the list.
     * @return number of tasks in the list
     */
    @Override
    public int getItemCount() {
        return tasks.size();
    }

    /**
     * Attach the list to the container view.
     * @param recyclerView recycler view that contain the list
     */
    @Override
    public void onAttachedToRecyclerView(@NotNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    /**
     * Task view holder class.
     */
    public static class TaskViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        ImageView itemIcon;
        TextView itemName;

        TaskViewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.list_item);
            itemIcon = itemView.findViewById(R.id.list_item_icon);
            itemName = itemView.findViewById(R.id.list_item_name);

        }
    }

}
