package com.example.routinity.ui.routines;

import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.routinity.R;
import com.example.routinity.structures.AbstractCondition;

import org.jetbrains.annotations.NotNull;

import java.util.List;


/**
 * Adapter for the condition list of a single routine in the routine list.
 */
public class RoutineConditionListAdapter extends RecyclerView.Adapter<RoutineConditionListAdapter.ConditionViewHolder> {

    List<AbstractCondition> conditions;

    /**
     * Constructor for the condition list adapter.
     * @param conditions list of conditions of the routine
     */
    public RoutineConditionListAdapter(List<AbstractCondition> conditions) {
        this.conditions = conditions;
    }

    /**
     * Create the view holder for the condition.
     * @param parent parent view
     * @param viewType view type
     * @return condition view holder
     */
    @NotNull
    @Override
    public RoutineConditionListAdapter.ConditionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ConditionViewHolder(view);
    }

    /**
     * Set conditions in the list.
     * @param holder condition holder
     * @param position position of the condition in the list
     */
    @Override
    public void onBindViewHolder(RoutineConditionListAdapter.ConditionViewHolder holder, int position) {
        holder.itemName.setText(conditions.get(position).getName());

        int id = holder.card.getResources().getIdentifier(conditions.get(position).getIcon(), "drawable", holder.card.getContext().getPackageName());
        @SuppressLint("UseCompatLoadingForDrawables") Drawable drawable = holder.card.getContext().getResources().getDrawable(id);
        holder.itemIcon.setImageDrawable(drawable);
    }

    /**
     * Get the number of conditions in the list.
     * @return number of conditions in the list
     */
    @Override
    public int getItemCount() {
        return conditions.size();
    }

    /**
     * Attach the list to the container view.
     * @param recyclerView recycler view that contain the list
     */
    @Override
    public void onAttachedToRecyclerView(@NotNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    /**
     * Condition view holder class.
     */
    public static class ConditionViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        ImageView itemIcon;
        TextView itemName;

        ConditionViewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.list_item);
            itemIcon = itemView.findViewById(R.id.list_item_icon);
            itemName = itemView.findViewById(R.id.list_item_name);

        }
    }
}
