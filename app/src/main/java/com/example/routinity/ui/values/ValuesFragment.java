package com.example.routinity.ui.values;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import com.example.routinity.R;

public class ValuesFragment extends Fragment {

    private ValuesViewModel valuesViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
            ViewGroup container, Bundle savedInstanceState) {
        valuesViewModel =
                new ViewModelProvider(this).get(ValuesViewModel.class);
        View root = inflater.inflate(R.layout.fragment_values, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_values);
        /*
        final TextView textView = root.findViewById(R.id.text_notifications);
        notificationsViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
       */
        return root;
    }
}