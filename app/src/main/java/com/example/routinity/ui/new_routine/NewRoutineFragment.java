package com.example.routinity.ui.new_routine;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.routinity.ConditionListActivity;
import com.example.routinity.R;
import com.example.routinity.TaskListActivity;


public class NewRoutineFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_new_routines, container, false);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.title_new_routine);

        Button addCondition = root.findViewById(R.id.add_condition);
        Button addTask = root.findViewById(R.id.add_task);

        addCondition.setOnClickListener(v -> {
            Intent myIntent = new Intent(getActivity(), ConditionListActivity.class);
            getActivity().startActivity(myIntent);
        });

        addTask.setOnClickListener(v -> {
            Intent myIntent = new Intent(getActivity(), TaskListActivity.class);
            getActivity().startActivity(myIntent);
        });

        return root;
    }
}