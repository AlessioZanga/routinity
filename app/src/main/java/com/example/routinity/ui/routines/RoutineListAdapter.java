package com.example.routinity.ui.routines;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.routinity.R;
import com.example.routinity.structures.Routine;
import com.google.android.material.switchmaterial.SwitchMaterial;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Adapter for the routine list.
 */
public class RoutineListAdapter extends RecyclerView.Adapter<RoutineListAdapter.RoutineViewHolder> {

    List<Routine> routines;
    List<Boolean> areExpanded;

    /**
     * Constructor for the routine list adapter.
     * @param routines list of routines
     */
    public RoutineListAdapter(List<Routine> routines) {
        this.routines = routines;
        this.areExpanded = new ArrayList<>(Collections.nCopies(routines.size(), false));
    }

    /**
     * Create the view holder for the routine.
     * @param parent parent view
     * @param viewType view type
     * @return Routine view holder
     */
    @NotNull
    @Override
    public RoutineViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.routine_item, parent, false);
        return new RoutineViewHolder(view);
    }

    /**
     * Set routines in the list.
     * @param holder routine holder
     * @param position position of the routine in the list
     */
    @Override
    public void onBindViewHolder(RoutineViewHolder holder, int position) {
        holder.routineName.setText(routines.get(position).getName());
        holder.routineNote.setText(routines.get(position).getNote());
        holder.enabled.setChecked(routines.get(position).isEnabled());

        RecyclerView conditionList = holder.card.findViewById(R.id.routine_conditions);
        conditionList.setLayoutManager(new LinearLayoutManager(holder.card.getContext()));
        conditionList.setAdapter(new RoutineConditionListAdapter(routines.get(position).getConditions()));

        RecyclerView taskList = holder.card.findViewById(R.id.routine_tasks);
        taskList.setLayoutManager(new LinearLayoutManager(holder.card.getContext()));
        taskList.setAdapter(new RoutineTaskListAdapter(routines.get(position).getTasks()));

        boolean isExpanded = areExpanded.get(position);
        holder.expandableList.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
    }

    /**
     * Get the number of routines in the list.
     * @return number of routines in the list
     */
    @Override
    public int getItemCount() {
        return routines.size();
    }

    /**
     * Attach the list to the container view.
     * @param recyclerView recycler view that contain the list
     */
    @Override
    public void onAttachedToRecyclerView(@NotNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    /**
     * Routine view holder class.
     */
    public class RoutineViewHolder extends RecyclerView.ViewHolder {
        CardView card;
        TextView routineName;
        TextView routineNote;
        SwitchMaterial enabled;
        LinearLayout routineGroup;
        ConstraintLayout expandableList;

        RoutineViewHolder(View itemView) {
            super(itemView);
            card = itemView.findViewById(R.id.routine_card);
            routineName = itemView.findViewById(R.id.routine_name);
            routineNote = itemView.findViewById(R.id.routine_note);
            enabled = itemView.findViewById(R.id.enable_routine);
            routineGroup = itemView.findViewById(R.id.routine_group);
            expandableList = itemView.findViewById(R.id.routine_expandable);

            routineGroup.setOnClickListener(v -> {
                Routine routine = routines.get(getAdapterPosition());
                areExpanded.set(getAdapterPosition(), !areExpanded.get(getAdapterPosition()));
                notifyItemChanged(getAdapterPosition());
            });
        }
    }
}
