package com.example.routinity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.routinity.foreground.ForegroundService;
import com.google.android.material.bottomnavigation.BottomNavigationView;

/**
 * Class for Main Activity.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Called when the activity is first created. The layout is linked to the activity. The
     * Top App Bar (Toolbar) and the Bottom navigation menu are set up. The navigation between
     * fragments is created and attached to the Bottom navigation menu.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView bottomNavigationMenu = findViewById(R.id.bottom_navigation_menu);
        bottomNavigationMenu.setItemIconTintList(null);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_routines, R.id.navigation_new_routines, R.id.navigation_values)
                .build();

        NavHostFragment navHostFragment = (NavHostFragment) getSupportFragmentManager()
                .findFragmentById(R.id.nav_host_fragment);
        NavController navController = navHostFragment.getNavController();
        NavigationUI.setupWithNavController(bottomNavigationMenu, navController);

        Toolbar myToolbar = findViewById(R.id.topAppBar);
        setSupportActionBar(myToolbar);

        // Call function to start the Foreground Service
        startService();
    }

    /* Start the Foreground Service */
    public void startService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        ContextCompat.startForegroundService(this, serviceIntent);
    }

    /* Stop the Foreground Service */
    public void stopService() {
        Intent serviceIntent = new Intent(this, ForegroundService.class);
        stopService(serviceIntent);
    }

    /**
     * Create the Top App Bar menu
     *
     * @param menu
     * @return true if the options of the top app menu is created
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_bar_menu, menu);
        return true;
    }

    /**
     * Actions to be executed when a button in the Top App Bar is selected
     *
     * @param item
     * @return true if the setting activity is started, false otherwise
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.top_app_bar_settings) {
            Intent myIntent = new Intent(MainActivity.this, SettingsActivity.class);
            MainActivity.this.startActivity(myIntent);
            return true;
        }
        return false;
    }
}