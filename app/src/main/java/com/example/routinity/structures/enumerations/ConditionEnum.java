package com.example.routinity.structures.enumerations;

import org.jetbrains.annotations.NotNull;

/**
 * Enum for the condition
 */
public enum ConditionEnum {
    APPLICATION("Applications", "apps_icon"),
    BATTERY_POWER("Battery/Power", "battery_icon"),
    CALL_SMS("Call/SMS", "phone_icon"),
    CONNECTIVITY("Connectivity", "connectivity_icon"),
    DATE_TIME("Date/Time", "date_icon"),
    DEVICE_EVENTS("Device Events", "device_events_icon"),
    LOCATION("Location", "location_icon"),
    SENSORS("Sensors", "sensors_icon"),
    USER_INPUT("User Input", "user_input_icon");

    private final String text;
    private final String icon;

    /**
     * Constructor of the ConditionEnum class
     * @param text text of the enum value
     */
    ConditionEnum(final String text, final String icon) {
        this.text = text;
        this.icon = icon;
    }

    /**
     * Getter of the string resource of the icon
     * @return resource string of the icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Get the text representation of the enum value
     * @return text of the enum value
     */
    @NotNull
    @Override
    public String toString() {
        return text;
    }
}
