package com.example.routinity.structures;

import android.content.Context;

import com.example.routinity.structures.enumerations.TaskEnum;

import java.util.List;


/**
 * Abstract class for Task structure.
 */
public abstract class AbstractTask {

    private String name;
    private String icon;
    private String description;
    private TaskEnum category;
    private List<String> permissions;

    /**
     * Abstract constructor of the AbstractTask class.
     *
     * @param name name of the task
     * @param icon icon of the task
     * @param description description of the task
     * @param category category of the task
     * @param permissions permissions associated with the task
     */
    public AbstractTask(String name, String icon, String description, TaskEnum category, List<String> permissions) {
        this.name = name;
        this.icon = icon;
        this.description = description;
        this.category = category;
        this.permissions = permissions;
    }

    /**
     * Getter of name attribute.
     * @return name of the task
     */
    public String getName() {
        return name;
    }

    /**
     * Getter of icon attribute.
     * @return icon of the task
     */
    public String getIcon() {
        return icon; // Drawable d = getResources().getDrawable(R.drawable.ICON, this.getTheme());
    }

    /**
     * Getter of description attribute.
     * @return description of the task
     */
    public String getDescription() {
        return description;
    }

    /**
     * Getter of category attribute.
     * @return category of the task
     */
    public TaskEnum getCategory() {
        return category;
    }

    /**
     * Getter of permission list.
     * @return permissions associated with the task
     */
    public List<String> getPermissions() {
        return permissions;
    }

    /**
     * Run the action related to the specific task.
     * @param context context of the activity
     * @return true if task run successfully, false otherwise
     */
    public abstract boolean run(Context context);
}
