package com.example.routinity.structures.exceptions;

/**
 * Exception for empty task list in a routine
 */
public class EmptyTasksException extends Exception {
}
