package com.example.routinity.structures.exceptions;

/**
 * Exception for empty condition list in a routine
 */
public class EmptyConditionsException extends Exception {
}
