package com.example.routinity.structures.enumerations;

/**
 * Enum for execution policy of routines
 */
public enum ExecPolicyEnum {

    IGNORE_FAILS,
    HALT_IF_FAIL
}
