package com.example.routinity.structures;

import com.example.routinity.structures.enumerations.ConditionEnum;

import java.util.List;



/**
 * Abstract class for Condition structure.
 */
public abstract class AbstractCondition {

    private String name;
    private String icon;
    private String description;
    private ConditionEnum category;
    private List<String> permissions;

    /**
     * Abstract constructor of the AbstractTask class.
     *
     * @param name name of the condition
     * @param icon icon of the condition
     * @param description description of the condition
     * @param category category of the condition
     * @param permissions permissions associated with the condition
     */
    public AbstractCondition(String name, String icon, String description, ConditionEnum category,
                        List<String> permissions) {
        this.name = name;
        this.icon = icon;
        this.description = description;
        this.category = category;
        this.permissions = permissions;
    }

    /**
     * Getter of name attribute.
     * @return name of the condition
     */
    public String getName() {
        return name;
    }

    /**
     * Getter of icon attribute.
     * @return icon of the condition
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Getter of description attribute.
     * @return description of the condition
     */
    public String getDescription() {
        return description;
    }

    /**
     * Getter of category attribute.
     * @return category of the condition
     */
    public ConditionEnum getCategory() {
        return category;
    }

    /**
     * Getter of permission list.
     * @return permissions associated with the condition
     */
    public List<String> getPermissions() {
        return permissions;
    }

    /**
     * Evaluate the condition.
     * @return true if the condition is verified, false otherwise
     */
    public abstract boolean eval();
}
