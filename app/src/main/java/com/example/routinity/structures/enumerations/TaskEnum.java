package com.example.routinity.structures.enumerations;

import org.jetbrains.annotations.NotNull;

/**
 * Enum for the task categories.
 */
public enum TaskEnum {
    APPLICATION("Applications", "apps_icon"),
    CAMERA("Camera", "camera_icon"),
    CONNECTIVITY("Connectivity", "connectivity_icon"),
    DATE_TIME("Date/Time", "date_icon"),
    DEVICE_ACTION("Device Actions", "device_action_icon"),
    DEVICE_SETTINGS("Device Settings", "settings_icon"),
    FILES("Files", "files_icon"),
    LOCATION("Location", "location_icon"),
    MEDIA("Media", "media_icon"),
    MESSAGING("Messaging", "message_icon"),
    NOTIFICATION("Notification", "notifications_icon"),
    PHONE("Phone", "phone_icon"),
    SCREEN("Screen", "screen_icon"),
    VOLUME("Volume", "volume_icon");

    private final String text;
    private final String icon;

    /**
     * Constructor of the TaskEnum class
     * @param text text of the enum value
     * @param icon resource string of the icon
     */
    TaskEnum(final String text, String icon) {
        this.text = text;
        this.icon = icon;
    }

    /**
     * Getter of the string resource of the icon
     * @return resource string of the icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * Get the text representation of the enum value
     * @return text of the enum value
     */
    @NotNull
    @Override
    public String toString() {
        return text;
    }
}
