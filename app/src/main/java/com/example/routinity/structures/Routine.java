package com.example.routinity.structures;

import android.content.Context;

import com.example.routinity.structures.enumerations.ExecPolicyEnum;
import com.example.routinity.structures.exceptions.EmptyConditionsException;
import com.example.routinity.structures.exceptions.EmptyNameException;
import com.example.routinity.structures.exceptions.EmptyTasksException;

import java.util.List;


/**
 * Routines class
 */
public class Routine {

    private String name;
    private String note;
    private boolean enabled;
    private ExecPolicyEnum execPolicy;
    private List<AbstractCondition> conditions;
    private List<AbstractTask> tasks;


    /**
     * Constructor for routine class
     * @param name name of the routine
     * @param note notes for the routine created by the user
     * @param execPolicy execution policy
     * @param conditions list of conditions
     * @param tasks list of tasks
     * @throws EmptyNameException exception for empty name
     */
    public Routine(String name, String note, ExecPolicyEnum execPolicy, List<AbstractCondition> conditions, List<AbstractTask> tasks) throws EmptyNameException {
        setName(name);
        setNote(note);
        setEnabled(true);
        setExecPolicy(execPolicy);
        setConditions(conditions);
        setTasks(tasks);
    }

    /**
     * Getter for the name of the routine
     * @return name of the routine
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the name of the routine. The name can not be void or with only spaces
     * @param name name for the routine
     */
    public void setName(String name) throws EmptyNameException {
        if(name == null || name.trim().isEmpty())
            throw new EmptyNameException();
        this.name = name;
    }

    /**
     * Getter for the notes of the routine
     * @return note of the routine
     */
    public String getNote() {
        return note;
    }

    /**
     * Setter for the notes of the routine
     * @param note note for the routine
     */
    public void setNote(String note) {
        this.note = note;
    }

    /**
     * Getter for the enabled/disabled status of the routine
     * @return true if the routine is enabled, false otherwise
     */
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Setter for the enabled/disabled state of the routine
     * @param enabled enable/disabled state of the routine
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    /**
     * Getter for the execution policy of the routine
     * @return execution policy of the routine
     */
    public ExecPolicyEnum getExecPolicy() {
        return execPolicy;
    }

    /**
     * Setter for the execution policy of the routine
     * @param execPolicy execution policy for the routine
     */
    public void setExecPolicy(ExecPolicyEnum execPolicy) {
        this.execPolicy = execPolicy;
    }

    /**
     * Getter for the conditions of the routine
     * @return conditions of the routine
     */
    public List<AbstractCondition> getConditions() {
        return conditions;
    }

    /**
     * Setter for the conditions of the routine, the conditions can not be an empty list
     * @param conditions conditions for the routine
     */
    public void setConditions(List<AbstractCondition> conditions) {
        this.conditions = conditions;
    }

    /**
     * Add a condition to the conditions of the routine
     * @param condition condition to add to the conditions
     * @return true if the condition is added successfully, false otherwise
     */
    public boolean addCondition(AbstractCondition condition){
        return conditions.add(condition);
    }

    /**
     * Remove a condition from the conditions of the routine
     * @param condition condition to remove from the conditions
     * @return true if the condition is removed successfully, false otherwise
     */
    public boolean removeCondition(AbstractCondition condition){
        return conditions.remove(condition);
    }

    /**
     * Getter for the tasks of the routine
     * @return tasks of the routine
     */
    public List<AbstractTask> getTasks() {
        return tasks;
    }

    /**
     * Setter for the tasks of the routine, the tasks can not be an empty list
     * @param tasks tasks for the routine
     */
    public void setTasks(List<AbstractTask> tasks) {
        this.tasks = tasks;
    }

    /**
     * Add a task to the tasks of the routine
     * @param task task to add to the tasks
     * @return true if the task is added successfully, false otherwise
     */
    public boolean addTask(AbstractTask task){
        return tasks.add(task);
    }

    /**
     * Remove a task from the tasks of the routine
     * @param task task to remove from the tasks
     * @return true if the task is removed successfully, false otherwise
     */
    public boolean removeTask(AbstractTask task){
        return tasks.remove(task);
    }

    /**
     * Run the tasks of the routine only if all the conditions are true
     * @param context context in which the method run
     * @return true if the method run successfully, false otherwise
     */
    public boolean execute(Context context){
        if (eval())
            return run(context);
        return false;
    }

    /**
     * Run the tasks of the routine, even if the conditions are false
     * @param context context in which the method run
     * @return true if the method run successfully, false otherwise
     */
    public boolean run(Context context){
        for (AbstractTask task : tasks){
            if (!task.run(context) && execPolicy == ExecPolicyEnum.HALT_IF_FAIL)
                return false;
        }
        return true;
    }

    /**
     * Evalutate all the conditions
     * @return true if all the conditions are true, false otherwise
     */
    public boolean eval(){
        for (AbstractCondition condition : conditions){
            if (!condition.eval())
                return false;
        }
        return true;
    }
}
