package com.example.routinity.structures.exceptions;


/**
 * Exception for empty name of a routine or name with only spaces
 */
public class EmptyNameException extends Throwable {
}
