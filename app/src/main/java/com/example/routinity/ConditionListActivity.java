package com.example.routinity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ExpandableListView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.example.routinity.structures.AbstractCondition;
import com.example.routinity.structures.enumerations.ConditionEnum;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import dalvik.system.DexFile;

/**
 * Class for Condition list Activity. In this Activity are reported all the conditions.
 */
public class ConditionListActivity extends AppCompatActivity {

    private Context context;
    private ExpandableListView expandableListView;
    private List<ConditionEnum> listGroup;
    private HashMap<ConditionEnum, List<AbstractCondition>> listItem;
    ConditionListAdapter adapter;

    /**
     * Called when the activity is first created. The layout is linked to the activity and the
     * Top app bar (Toolbar) is setted up
     *
     * @param savedInstanceState savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_condition_list);

        Toolbar myToolbar = findViewById(R.id.topAppBar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle(R.string.condition_list_title);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        context = this;
        expandableListView = findViewById(R.id.condition_list);
        listGroup = new ArrayList<>();
        listItem = new HashMap<>();
        adapter = new ConditionListAdapter(context, listGroup, listItem);
        expandableListView.setAdapter(adapter);
        initListData();
    }

    /**
     * Create the Top App Bar menu
     *
     * @param menu menu
     * @return true if the options of the top app menu is created
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.top_bar_menu, menu);
        return true;
    }

    /**
     * Actions to be executed when a button in the Top App Bar is selected
     *
     * @param item clicked item
     * @return true if the activity is started, false otherwise
     */
    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.top_app_bar_settings:
                Intent myIntent = new Intent(ConditionListActivity.this, SettingsActivity.class);
                ConditionListActivity.this.startActivity(myIntent);
                return true;

            default:
                return false;
        }
    }

    /**
     * Initialize the list data taking condition classes from package "com.example.routinity.conditions."
     */
    private void initListData() {
        ConditionEnum[] yourEnums = ConditionEnum.values();
        // Create condition category
        for (ConditionEnum val : yourEnums){
            listGroup.add(val);
            listItem.put(val, new ArrayList<>());
        }
        // Create items for condition category
        try {
            DexFile df = new DexFile(context.getPackageCodePath());
            for (Enumeration<String> iter = df.entries(); iter.hasMoreElements();) {
                String s = iter.nextElement();
                if (s.contains("com.example.routinity.conditions.")) {
                    Constructor<?> c = Class.forName(s).getDeclaredConstructor();
                    c.setAccessible(true);
                    AbstractCondition condition = (AbstractCondition) c.newInstance();
                    listItem.computeIfAbsent(condition.getCategory(), k -> new ArrayList<>()).add(condition);
                }
            }
        } catch (IOException | InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
    }
}
