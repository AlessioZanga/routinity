package com.example.routinity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import com.example.routinity.foreground.ForegroundService;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class ForegroundServiceTest {

    /* Test if the ForegroundService is running or not */
    @Test
    public void isForegroundServiceRunning() {
        // Get app context
        Context appContext = InstrumentationRegistry.getInstrumentation().getTargetContext();
        // Define an intent to run the ForegroundService
        Intent intent = new Intent(appContext, ForegroundService.class);
        // Start the ForegroundService
        appContext.startForegroundService(intent);
        // Define an Activity Manager to get all the running activities
        ActivityManager am = (ActivityManager) appContext.getSystemService(ACTIVITY_SERVICE);
        // Get a list of the running activities
        List<ActivityManager.RunningServiceInfo> runningServiceList = am.getRunningServices(50);
        // Define variable to check if the ForegroundService is running
        boolean serviceRunning = false;

        // Check if the ForegroundService is in the list of the running services
        for(ActivityManager.RunningServiceInfo runningServiceInfo : runningServiceList) {
            // Check if the ForegroundService is running
            if (runningServiceInfo.service.getClassName()
                    .equals("com.example.routinity.foreground.ForegroundService")) {
                // The ForegroundService is running
                serviceRunning = true;
                break;
            }
        }
        // Test assertion
        assertTrue(serviceRunning);
    }
}
