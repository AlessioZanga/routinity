package com.example.routinity;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.routinity.tasks.MobileDataOnTask;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class MobileDataOnTaskTest {

    @Test
    public void testRun() {
        assertTrue((new MobileDataOnTask()).run(ApplicationProvider.getApplicationContext()));
    }

}
