package com.example.routinity;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityNavigationTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void checkValuesNavigation() {
        onView(withId(R.id.navigation_values))
                .perform(click());
        onView(withId(R.id.topAppBar)).check(matches(hasDescendant(withText(R.string.title_values))));
    }
    @Test
    public void checkNewRoutineNavigation() {
        onView(withId(R.id.navigation_new_routines))
                .perform(click());
        onView(withId(R.id.topAppBar)).check(matches(hasDescendant(withText(R.string.title_new_routine))));
    }
    @Test
    public void checkRoutinesNavigation() {
        onView(withId(R.id.navigation_routines))
                .perform(click());
        onView(withId(R.id.topAppBar)).check(matches(hasDescendant(withText(R.string.title_routines))));
    }

    @Test
    public void checkSettingsNavigation() {
        onView(withId(R.id.top_app_bar_settings))
                .perform(click());
        onView(withId(R.id.topAppBar)).check(matches(hasDescendant(withText(R.string.settings))));
    }
}
