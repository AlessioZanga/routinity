package com.example.routinity;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;


@RunWith(AndroidJUnit4.class)
public class RoutineListTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule = new ActivityScenarioRule<>(MainActivity.class);

    @Before
    public void checkRoutinesNavigation() {
        onView(withId(R.id.navigation_routines))
                .perform(click());
        onView(withId(R.id.topAppBar)).check(matches(hasDescendant(withText(R.string.title_routines))));
    }

    @Test
    public void checkSettingsNavigation() {
        onView(withId(R.id.top_app_bar_settings))
                .perform(click());
        onView(withId(R.id.topAppBar)).check(matches(hasDescendant(withText(R.string.settings))));
    }

    @Test
    public void checkEmptyList() {
        onView(withId(R.id.routine_list)).check(matches(not(isDisplayed())));
        onView(withId(R.id.empty_view_card)).check(matches(isDisplayed()));
        onView(withId(R.id.empty_view_card)).check(matches(hasDescendant(withText(R.string.empty_routines))));
    }

}
