package com.example.routinity;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.enumerations.TaskEnum;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import dalvik.system.DexFile;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.scrollTo;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.hasChildCount;
import static androidx.test.espresso.matcher.ViewMatchers.hasDescendant;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anyOf;

@RunWith(AndroidJUnit4.class)
public class TaskListTest {

    private List<TaskEnum> listGroup;
    private HashMap<TaskEnum, List<AbstractTask>> listItem;

    @Rule
    public ActivityScenarioRule<TaskListActivity> activityRule = new ActivityScenarioRule<>(TaskListActivity.class);

    @Test
    public void checkSettingsNavigation() {
        onView(withId(R.id.top_app_bar_settings))
                .perform(click());
        onView(withId(R.id.topAppBar)).check(matches(hasDescendant(withText(R.string.settings))));
    }

    @Test
    public void checkListGroupNumbers(){
        // Check that the number of groups is correct (number of enumerations in TaskEnum
        listGroup = new ArrayList<>();
        Collections.addAll(listGroup, TaskEnum.values());
        onView(withId(R.id.task_list)).check(matches(hasChildCount(listGroup.size())));
    }

    @Test
    public void checkGroupsName(){
        // Check group names
        listGroup = new ArrayList<>();
        TaskEnum[] yourEnums = TaskEnum.values();
        for (TaskEnum val : yourEnums){
            onView(withText(String.valueOf(val)));
        }
    }

    @Test
    public void checkChildrenName() {
        // Check children for every group
        listGroup = new ArrayList<>();
        listItem = new HashMap<>();
        TaskEnum[] yourEnums = TaskEnum.values();
        for (TaskEnum val : yourEnums) {
            listGroup.add(val);
            listItem.put(val, new ArrayList<>());
        }

        activityRule.getScenario().onActivity(activity -> {
            try {
                DexFile df = new DexFile(activity.getPackageCodePath());
                for (Enumeration<String> iter = df.entries(); iter.hasMoreElements(); ) {
                    String s = iter.nextElement();
                    if (s.contains("com.example.routinity.tasks.")) {
                        Constructor<?> c = Class.forName(s).getDeclaredConstructor();
                        c.setAccessible(true);
                        AbstractTask task = (AbstractTask) c.newInstance();
                        listItem.computeIfAbsent(task.getCategory(), k -> new ArrayList<>()).add(task);
                    }
                }
            } catch (IOException | InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | ClassNotFoundException e) {
                e.printStackTrace();
            }
        });
        for (TaskEnum val : listGroup) {
            onView(withText(String.valueOf(val))).perform(scrollTo(), click());
            for (AbstractTask task : Objects.requireNonNull(listItem.get(val))) {
                onView(anyOf(withText(task.getName()))).check(matches(isDisplayed()));
            }
            onView(withText(String.valueOf(val))).perform(click());

        }
    }
}
