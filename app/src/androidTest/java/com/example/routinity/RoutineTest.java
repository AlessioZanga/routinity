package com.example.routinity;

import android.annotation.SuppressLint;
import android.content.Context;

import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.routinity.structures.AbstractCondition;
import com.example.routinity.structures.AbstractTask;
import com.example.routinity.structures.Routine;
import com.example.routinity.structures.enumerations.ConditionEnum;
import com.example.routinity.structures.enumerations.ExecPolicyEnum;
import com.example.routinity.structures.enumerations.TaskEnum;
import com.example.routinity.structures.exceptions.EmptyNameException;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@RunWith(AndroidJUnit4.class)
public class RoutineTest {

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule = new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void createRoutine() throws EmptyNameException {

        ArrayList<AbstractCondition> conditions = new ArrayList<>();
        conditions.add(new Condition1());
        conditions.add(new Condition2());
        ArrayList<AbstractTask> tasks = new ArrayList<>();
        tasks.add(new Task1());
        tasks.add(new Task2());

        Routine routine = new Routine(
                "Routine1",
                "Note for routine 1",
                ExecPolicyEnum.IGNORE_FAILS,
                conditions,
                tasks
                );

        assertEquals("Routine1", routine.getName());
        assertEquals("Note for routine 1", routine.getNote());
        assertEquals(ExecPolicyEnum.IGNORE_FAILS, routine.getExecPolicy());
        assertEquals(conditions, routine.getConditions());
        assertEquals(tasks, routine.getTasks());
    }

    @Test(expected = EmptyNameException.class)
    public void emptyName() throws EmptyNameException {
        Routine routine = new Routine(
                "",
                "Note for routine 1",
                ExecPolicyEnum.IGNORE_FAILS,
                new ArrayList<>(Arrays.asList(new Condition1(), new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
    }

    @Test(expected = EmptyNameException.class)
    public void NameWithOnlySpaces() throws  EmptyNameException {
        Routine routine = new Routine(
                "   ",
                "Note for routine 1",
                ExecPolicyEnum.IGNORE_FAILS,
                new ArrayList<>(Arrays.asList(new Condition1(), new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
    }

    @Test
    public void runRoutineIgnoreFalsePolicy() throws EmptyNameException {

        Routine routine1 = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.IGNORE_FAILS,
                new ArrayList<>(Arrays.asList(new Condition1(), new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
        // There a false condition
        activityRule.getScenario().onActivity(activity -> assertFalse(routine1.execute(activity.getBaseContext())));

        Routine routine2 = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.IGNORE_FAILS,
                new ArrayList<>(Arrays.asList(new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
        // All conditions are true and ignore fails policy execute all tasks even if there are some
        // of these that return false
        activityRule.getScenario().onActivity(activity -> assertTrue(routine2.execute(activity.getBaseContext())));
    }

    @Test
    public void runRoutineHaltIfFailPolicy() throws EmptyNameException {

        Routine routine1 = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList(new Condition1(), new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
        // There a false condition
        activityRule.getScenario().onActivity(activity -> assertFalse(routine1.execute(activity.getBaseContext())));

        Routine routine2 = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList(new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
        // All conditions are true and halt if fail policy block all tasks after there is one of
        // this that return false
        activityRule.getScenario().onActivity(activity -> assertFalse(routine2.execute(activity.getBaseContext())));

        Routine routine3 = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList(new Condition2())),
                new ArrayList<>(Arrays.asList(new Task2()))
        );
        // All conditions are true and all tasks are executed
        activityRule.getScenario().onActivity(activity -> assertTrue(routine3.execute(activity.getBaseContext())));
    }

    @Test
    public void run_tasks() throws EmptyNameException {

        Routine routine1 = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.IGNORE_FAILS,
                new ArrayList<>(Arrays.asList(new Condition1(), new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
        // There a false condition but we want to run the tasks in any case
        activityRule.getScenario().onActivity(activity -> assertTrue(routine1.run(activity.getBaseContext())));

        Routine routine2 = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList(new Condition1(), new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
        // There a false condition but we want to run the tasks in any case. However one task return
        // false and the halt if fail condition is selected
        activityRule.getScenario().onActivity(activity -> assertFalse(routine2.run(activity.getBaseContext())));
    }

    @Test
    public void routineIsEnabled() throws EmptyNameException {

        Routine routine = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.IGNORE_FAILS,
                new ArrayList<>(Arrays.asList(new Condition1(), new Condition2())),
                new ArrayList<>(Arrays.asList(new Task1(), new Task2()))
        );
        assertTrue(routine.isEnabled());
        routine.setEnabled(false);
        assertFalse(routine.isEnabled());
    }

    @Test
    public void addTask() throws EmptyNameException {
        Routine routine = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList()),
                new ArrayList<>(Arrays.asList())
        );
        assertTrue(routine.addTask(new Task1()));
    }

    @Test
    public void removeTask() throws EmptyNameException {
        Task1 task = new Task1();
        Routine routine = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList()),
                new ArrayList<>(Arrays.asList(task))
        );
        assertTrue(routine.removeTask(task));
        assertFalse(routine.removeTask(task));
    }

    @Test
    public void addCondition() throws EmptyNameException {
        Routine routine = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList()),
                new ArrayList<>(Arrays.asList())
        );
        assertTrue(routine.addCondition(new Condition1()));
    }

    @Test
    public void removeCondition() throws EmptyNameException {
        Condition1 condition = new Condition1();
        Routine routine = new Routine(
                "Routine",
                "Note for routine",
                ExecPolicyEnum.HALT_IF_FAIL,
                new ArrayList<>(Arrays.asList(condition)),
                new ArrayList<>(Arrays.asList())
        );
        assertTrue(routine.removeCondition(condition));
        assertFalse(routine.removeCondition(condition));
    }
}

class Condition1 extends AbstractCondition {

    public Condition1() {
        super(
                "Dummy Condition 1",
                "ic_launcher_foreground",
                "description dummy condition 1",
                ConditionEnum.APPLICATION,
                new ArrayList<>(Arrays.asList("PERMESSO3", "PERMESSO2"))
        );
    }
    @Override
    public boolean eval() {
        return false;
    }
}

class Condition2 extends AbstractCondition {

    public Condition2() {
        super(
                "Dummy Condition 2",
                "ic_launcher_foreground",
                "description dummy condition 2",
                ConditionEnum.APPLICATION,
                new ArrayList<>(Arrays.asList("PERMESSO1", "PERMESSO2"))
        );
    }
    @Override
    public boolean eval() {
        return true;
    }
}

class Task1 extends AbstractTask {
    @SuppressLint("UseCompatLoadingForDrawables")
    public Task1() {
        super(
                "Dummy Task 1",
                "ic_launcher_foreground",
                "description dummy task 1",
                TaskEnum.APPLICATION,
                new ArrayList<>(Arrays.asList("PERMESSO1", "PERMESSO2"))
        );
    }
    @Override
    public boolean run(Context context) {
        return false;
    }
}

class Task2 extends AbstractTask {
    @SuppressLint("UseCompatLoadingForDrawables")
    public Task2() {
        super(
                "Dummy Task 2",
                "ic_launcher_foreground",
                "description dummy task 2",
                TaskEnum.APPLICATION,
                new ArrayList<>(Arrays.asList("PERMESSO1", "PERMESSO2"))
        );
    }
    @Override
    public boolean run(Context context) {
        return true;
    }
}
