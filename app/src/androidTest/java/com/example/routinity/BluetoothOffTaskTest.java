package com.example.routinity;

import android.bluetooth.BluetoothAdapter;

import androidx.test.core.app.ApplicationProvider;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.example.routinity.tasks.BluetoothOffTask;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class BluetoothOffTaskTest {

    @Test
    public void testRun() {
        int bState = 0; // State of the Bluetooth adapter
        BluetoothAdapter bAdapter = BluetoothAdapter.getDefaultAdapter();   // Bluetooth adapter

        do {
            // Get current adapter state
            bState = bAdapter.getState();
            // If adapter is not on or not turning on
            if (bState != bAdapter.STATE_ON &&
                bState != bAdapter.STATE_TURNING_ON) {
                // Request to turn the adapter on
                bAdapter.enable();
            }
            // Repeat the request until the adapter is on
        } while(!bAdapter.isEnabled());

        // Request to turn the adapter off
        assertTrue((new BluetoothOffTask()).run(ApplicationProvider.getApplicationContext()));
    }

}
