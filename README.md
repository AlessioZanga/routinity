# Routinity

Componenti del gruppo:

Cavenaghi Emanuele  816385
Turano Andrea  816462
Zanga Alessio  815997

Descrizione del progetto:

Realizzazione di un'applicazione Android che consenta di eseguire un task o una sequenza di task in modo automatico.


## Architectural Spike

### Backend
This is a high level description of the main concepts on which the backend will be based. The Task object represents a specific operation that can be performed (e.g. switching on/off the WiFi, etc.), while the Condition can be essentially described an occurence of an event. A Routine is a composition of conditions and tasks. When all conditions are satisfied, the tasks are executed.

<img src="docs/backend.png"  width="50%" height="50%">

### Frontend
To implement the frontend of the application Android Studio is used. The "Routines" menu displays a list of all the existing routines. Each routine has a Name and two buttons that allow to modify and delete that specific routine. The "Add Routine" menu displays a tasks list which contains all the tasks that the routine has to execute and a condition list containing all the conditions that enable the routine. It is possible to delete a task or a condition with an apposite button besides each task/condition. The "Values" menu displays all the variables predefined by the user, which can be shared between multiple conditions. 

<img src="docs/frontend.png"  width="50%" height="50%">
